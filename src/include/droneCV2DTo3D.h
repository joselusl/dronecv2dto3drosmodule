//////////////////////////////////////////////////////
//  droneCV2DTo3D.h
//
//  Created on: Jul 25, 2014
//      Author: joselusl
//
//  Last modification on: Jul 25, 2014
//      Author: joselusl
//
//////////////////////////////////////////////////////

#ifndef DRONE_CV_2D_TO_3D_H
#define DRONE_CV_2D_TO_3D_H




//I/O stream
//std::cout
#include <iostream>

//Vector
//std::vector
#include <vector>

//String
//std::string, std::getline()
#include <string>

//String stream
//std::istringstream
#include <sstream>

//File Stream
//std::ofstream, std::ifstream
#include <fstream>


//OpenCV
#include "opencv2/opencv.hpp"


//Pose
#include "pose.h"
#include "poseOperations.h"



/////////////////////////////////////////
// Class DroneCV2DTo3D
//
//   Description
//
/////////////////////////////////////////
class DroneCV2DTo3D
{

    //Camera calibration parameters
protected:
    cv::Mat CameraMatrix;
    cv::Size CamSize;
    float SensorWidth, SensorHeight;

public:
    int setCameraCalibParameters(std::string fileName); //*.yaml


    //Pose of the camera in drone
protected:
    SL::Pose PoseCamera_wrt_Drone;
public:
    int setCameraPoseWRTDrone(std::string fileName); //*.yaml


    //Point in image plane
protected:
    cv::Point pointInImagePlane;
public:
    int setPoint2D(cv::Point pointInImagePlane);


    //Pose of the Drone in world
protected:
    SL::Pose dronePose;
public:
    int setDronePose(SL::Pose dronePose);
    int getDronePose(SL::Pose& dronePose);



    //Point in 3D
protected:
    cv::Point3d pointIn3DWorld;
public:
    int getPoint3D(cv::Point3d& pointIn3DWorld);


    //Run
public:
    int run();


};




#endif
