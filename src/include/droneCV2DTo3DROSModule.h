//////////////////////////////////////////////////////
//  droneCV2DTo3DROSModule.h
//
//  Created on: Jul 25, 2014
//      Author: joselusl
//
//  Last modification on: Jul 25, 2014
//      Author: joselusl
//
//////////////////////////////////////////////////////

#ifndef DRONE_CV_2D_TO_3D_ROS_MODULE_H
#define DRONE_CV_2D_TO_3D_ROS_MODULE_H




//I/O stream
//std::cout
#include <iostream>

//Vector
//std::vector
#include <vector>

//String
//std::string, std::getline()
#include <string>

//String stream
//std::istringstream
#include <sstream>

//File Stream
//std::ofstream, std::ifstream
#include <fstream>


//Core
#include "droneCV2DTo3D.h"


// ROS
#include "ros/ros.h"



//Drone module
#include "droneModuleROS.h"


//COMMON
//dronePoseStamped: Message in
#include "droneMsgsROS/dronePoseStamped.h"

//rotation angles
#include "geometry_msgs/Vector3Stamped.h"



//KEYPOINTS
//Keypoints: Messages in
#include <droneMsgsROS/vector2i.h>
#include <droneMsgsROS/vectorPoints2DInt.h>

//3D possition of keypoints: Message out
//Publishers
#include "droneMsgsROS/vector3f.h"
#include "droneMsgsROS/points3DStamped.h"



//Ground Robots: Messages in
#include <droneMsgsROS/targetInImage.h>
#include <droneMsgsROS/vectorTargetsInImageStamped.h>

//3D pose of ground robots: Message out
#include "droneMsgsROS/robotPose.h"
#include "droneMsgsROS/robotPoseVector.h"







/////////////////////////////////////////
// Class DroneCV2DTo3DROS
//
//   Description
//
/////////////////////////////////////////
class DroneCV2DTo3DROS
{
    //Camera config
protected:
    std::string camCalibParamFile;
public:
    int setCalibCamera(std::string camCalibParamFile);

    //Camera pose wrt drone
protected:
    std::string camPoseWRTDroneFile;
public:
    int setCameraPoseWRTDrone(std::string camPoseWRTDroneFile);


    //Drone Pose Topics name
public:
    int setDronePoseTopicConfigs(std::string dronePoseTopicName, std::string rotationAnglesTopicName);


    //subscribers: dronePose
protected:
    //Set
    bool dronePoseSetFlag;
    //Topic name
    std::string dronePoseTopicName; //GMR
    //Msg
    droneMsgsROS::dronePose dronePoseMsg; //TODO Buffer with time stamp
    //Subscriber
    ros::Subscriber dronePoseSubs;
public:
    void dronePoseCallback(const droneMsgsROS::dronePose::ConstPtr& msg); //yaw, x, y, z


    //subscribers: rotation angles: you should subscribe to dronePose to get rotation angles, but now, they are not working propertly
protected:
    //Set
    bool rotationAnglesFlag;
    //Topic name
    std::string droneRotationAnglesTopicName; //MavWork
    //Msg
    geometry_msgs::Vector3Stamped droneRotationAnglesMsg; //TODO Buffer with time stamp
    //Subscriber
    ros::Subscriber droneRotationAnglesSubs;
public:
    void droneRotationAnglesCallback(const geometry_msgs::Vector3Stamped::ConstPtr& msg); //pitch, roll



    //Main class
protected:
    DroneCV2DTo3D MyDroneCV2DTo3D;
protected:
    std::vector<cv::Point3d> pointsIn3DWorld; //GMR



public:
    DroneCV2DTo3DROS();
    ~DroneCV2DTo3DROS();


public:
    void open(ros::NodeHandle & nIn);

};







/////////////////////////////////////////
// Class DroneCVKeypoints2DTo3DROSModule
//
//   Description
//
/////////////////////////////////////////
class DroneCVKeypoints2DTo3DROSModule : public DroneModule, public DroneCV2DTo3DROS
{	
    //subscribers: keypoints in the image plane
    //Topic name
protected:
    std::string keypoints2DTopicName;
public:
    int setKeypoints2DTopicName(std::string keypoints2DTopicName);
protected:
    //Publisher
    ros::Subscriber keypoints2DSubs;
    //Msg
    droneMsgsROS::vectorPoints2DInt keypoints2DMsg;
public:
    //Callback
    void keypoints2DCallback(const droneMsgsROS::vectorPoints2DInt::ConstPtr& msg);


    //publishers: 3d pose of the keypoints
protected:
    //Topic name
    std::string keypoints3DPublTopicName;
public:
    int setKeypoints3DPublTopicName(std::string keypoints3DPublTopicName);
protected:
    //Publisher
    ros::Publisher droneKeypoints3DPubl;
    //Publish method
    bool publishKeypoints3D();


    //exchange info
protected:
    //std::vector<cv::Point3d> droneKeypoints3D;


public:
    DroneCVKeypoints2DTo3DROSModule();
    ~DroneCVKeypoints2DTo3DROSModule();
	
public:
    void open(ros::NodeHandle & nIn);
	void close();

protected:
    bool init();

    //Reset
protected:
    bool resetValues();

    //Start
protected:
    bool startVal();

    //Stop
protected:
    bool stopVal();

    //Run
public:
    bool run();


};




/////////////////////////////////////////
// Class DroneCVGroundRobots2DTo3DROSModule
//
//   Description
//
/////////////////////////////////////////
class DroneCVGroundRobots2DTo3DROSModule : public DroneModule, public DroneCV2DTo3DROS
{
protected:
    //subscribers: ground robots in the image plane
    //Topic name
protected:
    std::string groundRobots2DTopicName;
public:
    int setGroundRobots2DTopicName(std::string groundRobots2DTopicName);
protected:
    //Publisher
    ros::Subscriber groundRobots2DSubs;
    //Msg
    droneMsgsROS::vectorTargetsInImageStamped groundRobots2DMsg;
public:
    //Callback
    void groundRobots2DCallback(const droneMsgsROS::vectorTargetsInImageStamped::ConstPtr& msg);


    //publishers: 3d pose of the ground robots
protected:
    //Topic name
    std::string groundRobots3DPublTopicName;
public:
    int setGroundRobots3DPublTopicName(std::string groundRobots3DPublTopicName);
protected:
    //Publisher
    ros::Publisher droneGroundRobots3DPubl;
    //Publish method
    bool publishGroundRobots3D();


    //exchange info
protected:
    //std::vector<cv::Point3d> droneGroundRobotCenterPoint3D;


public:
    DroneCVGroundRobots2DTo3DROSModule();
    ~DroneCVGroundRobots2DTo3DROSModule();

public:
    void open(ros::NodeHandle & nIn);
    void close();

protected:
    bool init();

    //Reset
protected:
    bool resetValues();

    //Start
protected:
    bool startVal();

    //Stop
protected:
    bool stopVal();

    //Run
public:
    bool run();


};





#endif
